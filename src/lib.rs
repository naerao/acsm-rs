use std::{
    io::BufReader,
    path::Path,
};

use xml::reader::{EventReader, XmlEvent as ReaderEvent};

#[derive(thiserror::Error, Debug)]
pub enum AcsmError {
    #[error(transparent)]
    IO(#[from] std::io::Error),
}

/// Parsed acsm file
#[derive(Default, Debug)]
pub struct Acsm {
    /// Title of book
    pub title: Option<String>,
    /// Creator of book
    pub creator: Option<String>,
    /// Publisher of book
    pub publisher: Option<String>,
    /// Language of book
    pub language: Option<String>,
    /// File format of book
    pub format: Option<String>,
    /// Url used to fulfill acsm file
    pub operator_url: Option<String>,
}

impl Acsm {

    /// Parse Acsm from file
    pub fn from_file(acsm_path: &Path) -> Result<Self, AcsmError> {
        let file = std::fs::File::open(acsm_path)?;
        let reader = BufReader::new(file);
        return Self::parse_acsm(reader);
    }

    /// Parse Acsm from string
    pub fn from_string(acsm_string: &str) -> Result<Self, AcsmError> {
        Self::parse_acsm(acsm_string.as_bytes())
    }

    // Parse acsm file
    fn parse_acsm<T: std::io::Read>(reader: T) -> Result<Self, AcsmError> {
        let mut acsm = Self::default();
        let parser = EventReader::new(reader);
        let mut current = String::new();
        for event in parser {
            match event {
                Ok(ReaderEvent::StartElement { name, .. }) => {
                    current = name.local_name;
                },
                Ok(ReaderEvent::Characters(content)) => {
                    match current.as_str() {
                        "title" => acsm.title = Some(content),
                        "creator" => acsm.creator = Some(content),
                        "publisher" => acsm.publisher = Some(content),
                        "language" => acsm.language = Some(content),
                        "format" => acsm.format = Some(content),
                        "operatorURL" => acsm.operator_url = Some(content),
                        _ => (),
                    }
                },
                _ => ()
            }
        }
        return Ok(acsm);
    }

}
